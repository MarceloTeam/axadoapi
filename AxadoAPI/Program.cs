﻿using AxadoAPI.DAO;
using AxadoAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxadoAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            AxadoIntegration integracao = new AxadoIntegration();
            Sistema s = new Sistema();
            Consulta c = new Consulta();
            JsonRetorno retorno = new JsonRetorno();

            c.cep_origem = "04050000";
            c.cep_destino = "88020010";
            c.valor_notafiscal = "130.72";
            c.prazo_adicional = "1";
            c.preco_adicional = "1.00";

            List<Volumes> lstVolumes = new List<Volumes>();

            Volumes v = new Volumes();
            v.sku = "A-01";
            v.quantidade = "1";
            v.preco = "15,20";
            v.altura = "35";
            v.comprimento = "10";
            v.largura = "30";
            v.peso = "2";

            lstVolumes.Add(v);

            lstVolumes.Add(v);

            lstVolumes.Add(v);

            c.volumes = lstVolumes;

            s.SistemaUrl = "https://api.axado.com.br/v2/";
            s.SistemaModulo = "consulta";
            s.SistemaToken = "ce462e71c709c1641420464cfa8988ba";

            retorno = integracao.AxadoConsultaCEP(s, c);
        }
    }
}

