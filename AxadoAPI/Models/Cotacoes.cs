﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxadoAPI.Models
{
    public class Cotacoes
    {
        public string transportadora_metaname { get; set; }
        public string servico_logo { get; set; }
        public string cotacao_prazo { get; set; }
        public string servico_nome { get; set; }
        public string cotacao_preco { get; set; }
        public string servico_transporte { get; set; }
        public string cotacao_codigo { get; set; }
        public string servico_metaname { get; set; }
        public string cotacao_custo { get; set; }
    }
}
