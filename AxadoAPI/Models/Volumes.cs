﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxadoAPI.Models
{
    public class Volumes
    {
        public string sku { get; set; }
        public string quantidade { get; set; }
        public string preco { get; set; }
        public string altura { get; set; }
        public string comprimento { get; set; }
        public string largura { get; set; }
        public string peso { get; set; }
    }
}
