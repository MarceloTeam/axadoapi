﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxadoAPI.Models
{
    public class Consulta
    {
        public string cep_origem { get; set; }
        public string cep_destino { get; set; }
        public string valor_notafiscal { get; set; }
        public string prazo_adicional { get; set; }
        public string preco_adicional { get; set; }
        public List<Volumes> volumes { get; set; }
    }
}


