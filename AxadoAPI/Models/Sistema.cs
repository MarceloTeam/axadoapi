﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxadoAPI.Models
{
    public class Sistema
    {
        public string SistemaUrl { get; set; }
        public string SistemaUsuario { get; set; }
        public string SistemaToken { get; set; }
        public string SistemaModulo { get; set; }
    }
}
