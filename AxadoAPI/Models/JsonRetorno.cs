﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AxadoAPI.Models
{
    public class JsonRetorno
    {
        public List<Cotacoes> cotacoes { get; set; }
        public string consulta_token { get; set; }
    }
}
