﻿using AxadoAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AxadoAPI.DAO
{
    public class AxadoIntegration
    {
        public string AxadoGetAcessToken(Sistema s)
        {
            return s.SistemaToken;
        }

        public JsonRetorno AxadoConsultaCEP(Sistema s, Consulta c)
        {
            try
            {
                string result ="";
                JsonRetorno jReturn = new JsonRetorno();
                string json = "";

                /* Parametros Iniciais */
                json =
                    "    { " +
                    "    \"cep_origem\": \"" + c.cep_origem + "\", " +
                    "    \"cep_destino\": \"" + c.cep_destino + "\", " +
                    "    \"valor_notafiscal\": \"" + c.valor_notafiscal + "\", " +
                    "    \"prazo_adicional\": \"" + c.prazo_adicional + "\", " +
                    "    \"preco_adicional\": \"" + c.preco_adicional + "\", ";

                /* Parametros Volumes */

                json = json + "    \"volumes\": [ ";
                
                foreach (Volumes vol in c.volumes)
	            {
                    json = json +
                    "        { " +
                    "            \"sku\": \""+ vol.sku +"\", " +
                    "            \"quantidade\": \"" + vol.quantidade +"\", " +
                    "            \"preco\": \"" + vol.preco +"\", " +
                    "            \"altura\": \"" + vol.altura +"\", " +
                    "            \"comprimento\": \"" + vol.comprimento +"\", " +
                    "            \"largura\": \"" + vol.largura +"\", " +
                    "            \"peso\": \"" + vol.peso +"\" " +
                    "        } ";
	            }

                json = json + "     ] " +
                              "    } ";                    

                // turn our request string into a byte stream
                byte[] postBytes = Encoding.UTF8.GetBytes(json);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                string url = s.SistemaUrl + s.SistemaModulo + "?token=" + s.SistemaToken;

                HttpWebRequest webrequest = (HttpWebRequest)HttpWebRequest.Create(url);
                webrequest.Headers.Add("Authorization", s.SistemaToken);
                webrequest.Method = "POST";
                webrequest.ContentLength = postBytes.Length;
                webrequest.Credentials = CredentialCache.DefaultCredentials;
                webrequest.ContentType = "application/json;";
                
                using (var streamWriter = new StreamWriter(webrequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)webrequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();

                    jReturn = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<JsonRetorno>(result);
                }

                return jReturn;

            }
            catch (Exception ex)
            {
                return new JsonRetorno();
            }
        }

    }
}
